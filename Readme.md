**Installation**
-
- Clone project from repository to directory
- Go to project directory inside terminal
- Run composer update 

**Wordpress Database Configuration**
-
- Open config/database.php and modify database connection inside *wordpress* key to your own one

**Congratulation!!! You can now get all published post from wordpress with base_url/posts**

