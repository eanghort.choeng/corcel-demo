<div class="card">
    <div class="card-body">
        <h5 class="card-title">{{$post->title}}</h5>
        <h6 class="card-subtitle mb-2 text-muted">By: {{$post->author->user_nicename}}</h6>
        <div style="height: 150px; text-overflow: ellipsis; overflow: hidden">
            {!! $post->content !!}
        </div>
        <a class="btn-link" href="{{route('posts.show', ['post' => $post->ID])}}">Read more</a>
    </div>
</div>
