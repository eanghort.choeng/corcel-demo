@extends('layouts.master')

@section('content')
    <div class="container mt-5">
        <div class="row">
            @foreach($posts as $post)
                <div class="col-sm-3">
                    @include('components.post', $post)
                </div>
            @endforeach
        </div>
    </div>
@stop
