@extends('layouts.master')

@section('content')
    <div class="container">
        <h3>{{$post->title}}</h3>
        <h6>By {{$post->author->user_nicename}}</h6>
        {!! $post->content !!}
    </div>
@stop
